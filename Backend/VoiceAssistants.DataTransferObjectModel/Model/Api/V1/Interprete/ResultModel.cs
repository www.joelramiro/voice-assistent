﻿// <copyright file="InterpreteResultDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

using VoiceAssistant.Core.Interprete.LuisIntentService;

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.Interprete
{
    public class ResultModel
    {
        public object Value { get; set; }

        public TypeResult TypeResult { get; set; }
    }
}