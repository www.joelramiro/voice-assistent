﻿// <copyright file="InterpreteDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.Interprete
{
    using System.ComponentModel.DataAnnotations;

    public class InterpreteDataTransferObject
    {
        [Required]
        public string Text { get; set; }
    }
}
