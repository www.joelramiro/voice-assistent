﻿// <copyright file="HttpMethodAttribute.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.Interprete
{
    public enum HttpMethodAttribute
    {
        HttpGet,
        HttpPost
    }
}
