﻿// <copyright file="InterpreteResultDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

using VoiceAssistant.Core.Interprete.LuisIntentService;

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.Interprete
{
    public class InterpreteResultDataTransferObject
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public ResultModel ResultModel { get; set; }
    }
}
