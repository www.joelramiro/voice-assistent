﻿// <copyright file="ListHeaderDetailCreateDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.Api.V1.ListHeader
{
    public class ListHeaderDetailCreateDataTransferObject
    {
        public string Description { get; set; }
    }
}
