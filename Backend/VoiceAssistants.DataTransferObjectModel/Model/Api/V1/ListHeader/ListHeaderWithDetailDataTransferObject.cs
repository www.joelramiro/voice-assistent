﻿// <copyright file="ListHeaderWithDetailDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.ListHeader
{
    using System;
    using System.Collections.Generic;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.ListDetail;

    public class ListHeaderWithDetailDataTransferObject
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Description { get; set; }

        public IEnumerable<ListDetailDataTransferObject> Details { get; set; }
    }
}
