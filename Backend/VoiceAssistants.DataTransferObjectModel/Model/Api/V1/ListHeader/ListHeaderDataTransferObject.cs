﻿// <copyright file="ListHeaderDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.ListHeader
{
    using System;

    public class ListHeaderDataTransferObject
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Description { get; set; }
    }
}
