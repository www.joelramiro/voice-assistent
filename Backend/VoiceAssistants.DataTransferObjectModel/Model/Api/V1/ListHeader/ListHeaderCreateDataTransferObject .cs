﻿// <copyright file="ListHeaderDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.ListHeader
{
    using System.Collections.Generic;
    using VoiceAssistants.DataTransferObjectModel.Model.Api.V1.ListHeader;

    public class ListHeaderCreateDataTransferObject
    {
        public string Description { get; set; }

        public IEnumerable<ListHeaderDetailCreateDataTransferObject> Details { get; set; }
    }
}
