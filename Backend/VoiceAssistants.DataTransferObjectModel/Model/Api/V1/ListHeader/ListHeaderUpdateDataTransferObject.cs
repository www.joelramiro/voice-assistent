﻿// <copyright file="ListHeaderUpdateDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.ListHeader
{
    using System.ComponentModel.DataAnnotations;

    public class ListHeaderUpdateDataTransferObject
    {
        [Required]
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
