﻿// <copyright file="ListItemDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.GeneralData
{
    using System.Collections.Generic;

    public class ListItemDataTransferObject<TEntity>
    {
        public IEnumerable<TEntity> Value { get; set; }

    }
}
