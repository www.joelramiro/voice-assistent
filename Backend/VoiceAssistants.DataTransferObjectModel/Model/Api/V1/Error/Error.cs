﻿// <copyright file="Error.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.Error
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Error
    {
        [Required]
        public string Code { get; set; }

        [Required]
        public string Message { get; set; }

        public string Target { get; set; }

        public IEnumerable<Error> Details { get; set; }

        public InnerError InnerError { get; set; }
    }
}
