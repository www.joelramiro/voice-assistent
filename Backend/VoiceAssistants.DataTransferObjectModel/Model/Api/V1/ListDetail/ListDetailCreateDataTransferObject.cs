﻿// <copyright file="ListDetailCreateDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.ListDetail
{
    public class ListDetailCreateDataTransferObject
    {
        public int IdListHeader { get; set; }

        public string Description { get; set; }
    }
}
