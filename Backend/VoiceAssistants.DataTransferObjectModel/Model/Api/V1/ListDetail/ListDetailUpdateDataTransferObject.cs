﻿// <copyright file="ListDetailUpdateDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.ListDetail
{
    public class ListDetailUpdateDataTransferObject
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
