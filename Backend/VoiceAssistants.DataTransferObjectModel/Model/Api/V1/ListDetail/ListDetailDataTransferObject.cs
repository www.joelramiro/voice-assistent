﻿// <copyright file="ListDetailDataTransferObject.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistants.DataTransferObjectModel.Model.V1.ListDetail
{
    using System;

    public class ListDetailDataTransferObject
    {
        public int Id { get; set; }

        public int IdListHeader { get; set; }

        public string ListHeaderDescription { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Description { get; set; }
    }
}
