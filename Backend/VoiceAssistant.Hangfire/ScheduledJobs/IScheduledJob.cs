﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoiceAssistant.Hangfire.ScheduledJobs
{
    public interface IScheduledJob
    {
        public bool EnqueeMessage(DateTime time, string message);
    }
}
