﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoiceAssistant.Hangfire.ScheduledJobs
{
    public class ScheduledJob : IScheduledJob
    {
        private readonly IRecurringJobManager recurringJobManager;

        public ScheduledJob(IRecurringJobManager recurringJobManager)
        {
            this.recurringJobManager = recurringJobManager;
        }

        public bool EnqueeMessage(DateTime time, string message)
        {
            recurringJobManager.AddOrUpdate(
                $"New message at {DateTime.Now.ToShortDateString()}",
                () => Console.WriteLine(message),
                "* * * * *"
                );

            return true;
        }
    }
}
