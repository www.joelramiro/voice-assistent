﻿// <copyright file="ListHeaderRepository.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Database.Repositories
{
    using System;
    using System.Linq;
    using VoiceAssistant.Database.Database.Context;
    using VoiceAssistant.Database.Models;

    public class ListHeaderRepository : VoiceAssistantDbContextRepositoryBase<ListHeader>
    {
        public ListHeaderRepository(VoiceAssistantContext context)
            : base(context)
        {
        }

        public override IQueryable<ListHeader> All()
        {
            return this.Context.ListHeaders;
        }

        protected override ListHeader MapNewValuesToOld(ListHeader oldEntity, ListHeader newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
