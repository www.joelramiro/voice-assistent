﻿// <copyright file="ListDetailRepository.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Database.Repositories
{
    using System;
    using System.Linq;
    using VoiceAssistant.Database.Database.Context;
    using VoiceAssistant.Database.Models;

    public class ListDetailRepository : VoiceAssistantDbContextRepositoryBase<ListDetail>
    {
        public ListDetailRepository(VoiceAssistantContext context)
            : base(context)
        {
        }

        public override IQueryable<ListDetail> All()
        {
            return this.Context.ListDetails;
        }

        protected override ListDetail MapNewValuesToOld(ListDetail oldEntity, ListDetail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
