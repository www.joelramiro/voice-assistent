﻿// <copyright file="VoiceAssistantDbContextRepositoryBase.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Database.Repositories
{
    using VoiceAssistant.Database.Database.Context;

    public abstract class VoiceAssistantDbContextRepositoryBase<TEntity> : RepositoryBase<TEntity, VoiceAssistantContext>
        where TEntity : class
    {
        public VoiceAssistantDbContextRepositoryBase(VoiceAssistantContext context)
            : base(context)
        {
            this.Context = context;
        }
    }
}
