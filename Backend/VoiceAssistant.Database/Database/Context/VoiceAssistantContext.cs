﻿// <copyright file="VoiceAssistantContext.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Database.Database.Context
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using VoiceAssistant.Database.Models;

    public class VoiceAssistantContext : DbContext
    {
        public VoiceAssistantContext(DbContextOptions<VoiceAssistantContext> options)
            : base(options)
        {
        }

        public DbSet<ListHeader> ListHeaders { get; set; }

        public DbSet<ListDetail> ListDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ListHeader>().Property(k => k.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<ListDetail>().Property(k => k.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<ListHeader>().HasMany(p => p.Details).WithOne(c => c.ListHeader)
                .HasForeignKey(c => c.IdListHeader);

            modelBuilder.Entity<ListHeader>().HasData(new ListHeader
            {
                Id = -1,
                CreatedDate = DateTime.Now,
                Description = "Lista de comidas nacionales",
            });

            modelBuilder.Entity<ListDetail>().HasData(new ListDetail
            {
                Id = -1,
                IdListHeader = -1,
                CreatedDate = DateTime.Now,
                Description = "Baleadas"
            });

            modelBuilder.Entity<ListDetail>().HasData(new ListDetail
            {
                Id = -2,
                IdListHeader = -1,
                CreatedDate = DateTime.Now,
                Description = "Pollo chuco"
            });
        }
    }
}