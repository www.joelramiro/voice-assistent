﻿// <copyright file="ListHeader.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Database.Models
{
    using System;
    using System.Collections.Generic;

    public class ListHeader
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Description { get; set; }

        public ICollection<ListDetail> Details { get; set; } = new HashSet<ListDetail>();
    }
}
