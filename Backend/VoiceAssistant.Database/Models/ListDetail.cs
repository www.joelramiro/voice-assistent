﻿// <copyright file="ListDetail.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Database.Models
{
    using System;

    public class ListDetail
    {
        public int Id { get; set; }

        public int IdListHeader { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Description { get; set; }

        public ListHeader ListHeader { get; set; }
    }
}