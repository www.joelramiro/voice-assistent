﻿// <copyright file="IListDetailManager.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.ListDetail
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using VoiceAssistant.Core.Settings.Contabilidad.Core;

    public interface IListDetailManager
    {
        Task<ServiceResult<IEnumerable<Database.Models.ListDetail>>> GetAllAsync();

        Task<ServiceResult<IEnumerable<Database.Models.ListDetail>>> GetAllByListHeaderIdAsync(int listHeaderId);

        Task<ServiceResult<Database.Models.ListDetail>> GetByIdAsync(int id);

        Task<ServiceResult<bool>> CreateAsync(Database.Models.ListDetail listDetail);

        Task<ServiceResult<bool>> EditAsync(Database.Models.ListDetail listDetail);

        Task<ServiceResult<bool>> DeleteByIdAsync(int id);
    }
}
