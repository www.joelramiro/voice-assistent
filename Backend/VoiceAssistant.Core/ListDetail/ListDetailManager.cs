﻿// <copyright file="ListDetailManager.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.ListDetail
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using VoiceAssistant.Core.Settings.Contabilidad.Core;
    using VoiceAssistant.Database.Repositories;

    public class ListDetailManager : IListDetailManager
    {
        private readonly IRepository<Database.Models.ListDetail> listDetailRepository;
        private readonly IRepository<Database.Models.ListHeader> listHeaderRepository;

        public ListDetailManager(
            IRepository<Database.Models.ListDetail> listDetailRepository,
            IRepository<Database.Models.ListHeader> listHeaderRepository)
        {
            this.listDetailRepository = listDetailRepository;
            this.listHeaderRepository = listHeaderRepository;
        }

        public async Task<ServiceResult<bool>> CreateAsync(Database.Models.ListDetail listDetail)
        {
            if (listDetail == null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"The object {nameof(listDetail)} can't be null." });
            }

            var listHeader = await this.listHeaderRepository.FirstOrDefaultAsync(d => d.Id == listDetail.IdListHeader);
            if (listHeader == null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"Not found a List with the id [{listDetail.IdListHeader}]." });
            }

            var exist = await this.listDetailRepository.FirstOrDefaultAsync(d => d.Description == listDetail.Description && d.IdListHeader == listDetail.IdListHeader);
            if (exist != null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"A object with the name [{listDetail.Description}] already exist." });
            }

            try
            {
                var result = this.listDetailRepository.Create(listDetail);
                await this.listDetailRepository.SaveChangesAsync();
                return ServiceResult<bool>.SuccessResult(true);
            }
            catch (Exception)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"Exception at created the registred" });
            }
        }

        public async Task<ServiceResult<bool>> DeleteByIdAsync(int id)
        {
            var listDetail = await this.listDetailRepository.FirstOrDefaultAsync(d => d.Id == id);
            if (listDetail == null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"Not found a element with the id [{id}]." });
            }

            this.listDetailRepository.Delete(listDetail);
            await this.listDetailRepository.SaveChangesAsync();
            return ServiceResult<bool>.SuccessResult(true);
        }

        public async Task<ServiceResult<bool>> EditAsync(Database.Models.ListDetail listDetail)
        {
            if (listDetail == null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"The object {nameof(listDetail)} can't be null." });
            }

            var listDetailOriginal = await this.listDetailRepository.FirstOrDefaultAsync(d => d.Id == listDetail.Id);
            if (listDetailOriginal == null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"Not found a element with the id [{listDetail.Id}]." });
            }

            listDetailOriginal.Description = listDetail.Description;
            this.listDetailRepository.Update(listDetailOriginal);
            await this.listDetailRepository.SaveChangesAsync();

            return ServiceResult<bool>.SuccessResult(true);
        }

        public async Task<ServiceResult<IEnumerable<Database.Models.ListDetail>>> GetAllAsync()
        {
            var result = await this.listDetailRepository.All().ToListAsync();
            return ServiceResult<IEnumerable<Database.Models.ListDetail>>.SuccessResult(result);
        }

        public async Task<ServiceResult<IEnumerable<Database.Models.ListDetail>>> GetAllByListHeaderIdAsync(int listHeaderId)
        {
            var result = await this.listDetailRepository.Filter(d => d.IdListHeader == listHeaderId).ToListAsync();
            return ServiceResult<IEnumerable<Database.Models.ListDetail>>.SuccessResult(result);
        }

        public async Task<ServiceResult<Database.Models.ListDetail>> GetByIdAsync(int id)
        {
            var result = await this.listDetailRepository.FirstOrDefaultAsync(d => d.Id == id);
            if (result == null)
            {
                return ServiceResult<Database.Models.ListDetail>.ErrorResult(new[] { $"Not found a element with the id [{id}]." });
            }

            return ServiceResult<Database.Models.ListDetail>.SuccessResult(result);
        }
    }
}