﻿// <copyright file="ServiceResult.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.Linq;

namespace VoiceAssistant.Core.Settings
{// <copyright file="OperationResult.cs" company="Open Source">
 // Copyright (c) Open Source. All rights reserved.
 // </copyright>
    namespace Contabilidad.Core
    {
        public class ServiceResult<T>
        {
            public IReadOnlyDictionary<string, IEnumerable<string>> ValidationMessages { get; protected set; }

            public bool Succeeded { get; protected set; }

            public T Result { get; set; }

            public ServiceResult(T result, bool succeeded, Dictionary<string, IEnumerable<string>> errors)
            {
                this.Result = result;
                this.Succeeded = succeeded;
                this.ValidationMessages = errors;
            }

            public static ServiceResult<T> ErrorResult(string[] errors) =>
                new ServiceResult<T>(default(T), false, new Dictionary<string, IEnumerable<string>> { { string.Empty, errors.Select(e => e) } });

            public static ServiceResult<T> SuccessResult(T result) =>
                new ServiceResult<T>(result, true, new Dictionary<string, IEnumerable<string>>());
        }
    }
}