﻿// <copyright file="TypeResult.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Interprete.LuisIntentService
{
    public enum TypeResult
    {
        Boolean,
        ListHeader,
        ListHeaderWithDetail,
        ListDetail,
    }
}