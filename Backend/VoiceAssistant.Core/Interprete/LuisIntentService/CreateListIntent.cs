﻿// <copyright file="CreateListIntent.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Interprete.LuisIntentService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using VoiceAssistant.Core.ListHeader;
    using VoiceAssistant.Core.Models.Interprete;
    using VoiceAssistant.Core.Models.Interprete.Entities;
    using VoiceAssistant.Core.Settings.Contabilidad.Core;

    public class CreateListIntent : IIntentResult
    {
        private readonly IListHeaderManager listHeaderManager;

        public CreateListIntent(IListHeaderManager listHeaderManager)
        {
            this.listHeaderManager = listHeaderManager;
        }

        public string IntentName => "Crear_Lista";

        public TypeResult TypeResult => TypeResult.Boolean;

        public async Task<ServiceResult<InterpreteResult>> Execute(LuisResponse luisResponse)
        {
            if (luisResponse == null || luisResponse.Prediction == null || luisResponse.Prediction.Entities == null || !luisResponse.Prediction.Entities.List.Any())
            {
                return ServiceResult<InterpreteResult>.ErrorResult(new[] { "Error with result" });
            }

            var date = DateTime.Now;

            var result = await this.listHeaderManager.CreateAsync(new Database.Models.ListHeader
            {
                CreatedDate = date,
                Description = luisResponse.Prediction.Entities.List.First(),
                Details = luisResponse.Prediction.Entities.Ingredient == null ? new List<Database.Models.ListDetail>()
                : !luisResponse.Prediction.Entities.Ingredient.Any() ? new List<Database.Models.ListDetail>()
                : luisResponse.Prediction.Entities.Ingredient.Select(d => new Database.Models.ListDetail
                {
                    CreatedDate = date,
                    Description = d,
                }).ToList(),
            });

            if (!result.Succeeded)
            {
                var messages = result.ValidationMessages.Select(d => d.Value.First());
                return ServiceResult<InterpreteResult>.ErrorResult(messages.ToArray());
            }

            return ServiceResult<InterpreteResult>.SuccessResult(new InterpreteResult
            {
                IsSuccess = true,
                Message = $"Se registro correctamente la lista {luisResponse.Prediction.Entities.List.First()}",
                ResultModel = new ResultModel<object> { Value = true },
                TypeResult = this.TypeResult,
            });
        }
    }
}
