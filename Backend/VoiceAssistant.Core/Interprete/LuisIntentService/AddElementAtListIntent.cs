﻿// <copyright file="AddElementAtListIntent.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Interprete.LuisIntentService
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using VoiceAssistant.Core.ListHeader;
    using VoiceAssistant.Core.Models.Interprete;
    using VoiceAssistant.Core.Models.Interprete.Entities;
    using VoiceAssistant.Core.Settings.Contabilidad.Core;

    public class AddElementAtListIntent : IIntentResult
    {
        private readonly IListHeaderManager listHeaderManager;

        public AddElementAtListIntent(IListHeaderManager listHeaderManager)
        {
            this.listHeaderManager = listHeaderManager;
        }

        public string IntentName => "Add_element_at_list";

        public TypeResult TypeResult => TypeResult.Boolean;

        public async Task<ServiceResult<InterpreteResult>> Execute(LuisResponse luisResponse)
        {
            if (luisResponse == null || luisResponse.Prediction == null || luisResponse.Prediction.Entities == null || !luisResponse.Prediction.Entities.List.Any() || !luisResponse.Prediction.Entities.Ingredient.Any())
            {
                return ServiceResult<InterpreteResult>.ErrorResult(new[] { "Error with result" });
            }

            var date = DateTime.Now;
            var listName = luisResponse.Prediction.Entities.List.First();
            var list = await this.listHeaderManager.GetByNameAsync(listName);

            if (!list.Succeeded)
            {
                return ServiceResult<InterpreteResult>.ErrorResult(new[] { list.ValidationMessages.First().Value.First() });
            }

            if (list.Result.Details == null)
            {
                list.Result.Details = luisResponse.Prediction.Entities.Ingredient.Select(i => new Database.Models.ListDetail
                {
                    CreatedDate = date,
                    Description = i,
                }).ToList();
            }
            else
            {
                list.Result.Details = list.Result.Details.Union(luisResponse.Prediction.Entities.Ingredient.Distinct().Select(i => new Database.Models.ListDetail
                {
                    CreatedDate = date,
                    Description = i,
                })).ToList();
            }

            var result = await this.listHeaderManager.EditAsync(list.Result);

            if (!result.Succeeded)
            {
                var messages = result.ValidationMessages.Select(d => d.Value.First());
                return ServiceResult<InterpreteResult>.ErrorResult(messages.ToArray());
            }

            return ServiceResult<InterpreteResult>.SuccessResult(new InterpreteResult
            {
                IsSuccess = true,
                Message = $"Se agregaron correctamente el(los) ingrediente(s) a la lista {luisResponse.Prediction.Entities.List.First()}",
                ResultModel = new ResultModel<object> { Value = true },
            });
        }
    }
}
