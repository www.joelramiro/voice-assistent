﻿// <copyright file="ShowListDetailIntent.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Interprete.LuisIntentService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VoiceAssistant.Core.ListHeader;
    using VoiceAssistant.Core.Models.Interprete;
    using VoiceAssistant.Core.Models.Interprete.Entities;
    using VoiceAssistant.Core.Settings.Contabilidad.Core;

    public class ShowListHeaderWithDetailIntent : IIntentResult
    {
        private readonly IListHeaderManager listHeaderManager;

        public ShowListHeaderWithDetailIntent(IListHeaderManager listHeaderManager)
        {
            this.listHeaderManager = listHeaderManager;
        }

        public string IntentName => "Mostrar_lista_con_detalle";

        public TypeResult TypeResult => TypeResult.ListHeaderWithDetail;

        public async Task<ServiceResult<InterpreteResult>> Execute(LuisResponse luisResponse)
        {
            if (luisResponse == null || luisResponse.Prediction == null || luisResponse.Prediction.Entities == null || !luisResponse.Prediction.Entities.List.Any())
            {
                return ServiceResult<InterpreteResult>.ErrorResult(new[] { "Error with result" });
            }

            var result = await this.listHeaderManager.GetByNameAsync(luisResponse.Prediction.Entities.List.First());

            if (!result.Succeeded)
            {
                var messages = result.ValidationMessages.Select(d => d.Value.First());
                return ServiceResult<InterpreteResult>.ErrorResult(messages.ToArray());
            }

            return ServiceResult<InterpreteResult>.SuccessResult(new InterpreteResult
            {
                IsSuccess = true,
                Message = $"Se devuelve la lista con detalle correctamente",
                ResultModel = new ResultModel<object> { Value = result.Result },
                TypeResult = this.TypeResult,
            });
        }
    }
}
