﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoiceAssistant.Core.ListHeader;
using VoiceAssistant.Core.Models.Interprete;
using VoiceAssistant.Core.Models.Interprete.Entities;
using VoiceAssistant.Core.Settings.Contabilidad.Core;

namespace VoiceAssistant.Core.Interprete.LuisIntentService
{
    public class CleanListIntent : IIntentResult
    {
        private readonly IListHeaderManager listHeaderManager;

        public CleanListIntent(IListHeaderManager listHeaderManager)
        {
            this.listHeaderManager = listHeaderManager;
        }

        public string IntentName => "Clean_list";

        public TypeResult TypeResult => TypeResult.Boolean;

        public async Task<ServiceResult<InterpreteResult>> Execute(LuisResponse luisResponse)
        {
            if (luisResponse == null || luisResponse.Prediction == null || luisResponse.Prediction.Entities == null || !luisResponse.Prediction.Entities.List.Any())
            {
                return ServiceResult<InterpreteResult>.ErrorResult(new[] { "Error with result" });
            }

            var listName = luisResponse.Prediction.Entities.List.First();
            var list = await this.listHeaderManager.GetByNameAsync(listName);

            if (!list.Succeeded)
            {
                return ServiceResult<InterpreteResult>.ErrorResult(new[] { list.ValidationMessages.First().Value.First() });
            }

            list.Result.Details = new List<Database.Models.ListDetail>();

            var result = await this.listHeaderManager.EditAsync(list.Result);

            if (!result.Succeeded)
            {
                var messages = result.ValidationMessages.Select(d => d.Value.First());
                return ServiceResult<InterpreteResult>.ErrorResult(messages.ToArray());
            }

            return ServiceResult<InterpreteResult>.SuccessResult(new InterpreteResult
            {
                IsSuccess = true,
                Message = $"Se limpio la lista {luisResponse.Prediction.Entities.List.First()}",
                ResultModel = new ResultModel<object> { Value = true },
            });
        }
    }
}
