﻿// <copyright file="IIntentResult.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Interprete.LuisIntentService
{
    using System.Threading.Tasks;
    using VoiceAssistant.Core.Models.Interprete.Entities;
    using VoiceAssistant.Core.Settings.Contabilidad.Core;

    public interface IIntentResult
    {
        public string IntentName { get; }

        public Task<ServiceResult<Models.Interprete.InterpreteResult>> Execute(LuisResponse luisResponse);

        public TypeResult TypeResult { get; }
    }
}
