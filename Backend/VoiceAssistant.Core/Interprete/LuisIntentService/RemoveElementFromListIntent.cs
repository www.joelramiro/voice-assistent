﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoiceAssistant.Core.ListHeader;
using VoiceAssistant.Core.Models.Interprete;
using VoiceAssistant.Core.Models.Interprete.Entities;
using VoiceAssistant.Core.Settings.Contabilidad.Core;

namespace VoiceAssistant.Core.Interprete.LuisIntentService
{
    public class RemoveElementFromListIntent : IIntentResult
    {
        private readonly IListHeaderManager listHeaderManager;

        public RemoveElementFromListIntent(IListHeaderManager listHeaderManager)
        {
            this.listHeaderManager = listHeaderManager;
        }

        public string IntentName => "Remove_element_from_list";

        public TypeResult TypeResult => TypeResult.Boolean;

        public async Task<ServiceResult<InterpreteResult>> Execute(LuisResponse luisResponse)
        {
            if (luisResponse == null || luisResponse.Prediction == null || luisResponse.Prediction.Entities == null || !luisResponse.Prediction.Entities.List.Any() || !luisResponse.Prediction.Entities.Ingredient.Any())
            {
                return ServiceResult<InterpreteResult>.ErrorResult(new[] { "Error with result" });
            }

            var date = DateTime.Now;
            var listName = luisResponse.Prediction.Entities.List.First();
            var list = await this.listHeaderManager.GetByNameAsync(listName);

            if (!list.Succeeded)
            {
                return ServiceResult<InterpreteResult>.ErrorResult(new[] { list.ValidationMessages.First().Value.First() });
            }

            if (list.Result.Details == null)
            {
                return ServiceResult<InterpreteResult>.ErrorResult(new[] { $"Not found elements at list {listName}" });
            }
            else
            {
                var elements = luisResponse.Prediction.Entities.Ingredient;

                var exist = list.Result.Details.Any(d => elements.Contains(d.Description));
                if (!exist)
                {
                    return ServiceResult<InterpreteResult>.ErrorResult(new[] { $"Not found the element(s) at list {listName}" });
                }

                list.Result.Details = list.Result.Details.Where(d => !elements.Contains(d.Description)).ToList();
            }

            var result = await this.listHeaderManager.EditAsync(list.Result);

            if (!result.Succeeded)
            {
                var messages = result.ValidationMessages.Select(d => d.Value.First());
                return ServiceResult<InterpreteResult>.ErrorResult(messages.ToArray());
            }

            return ServiceResult<InterpreteResult>.SuccessResult(new InterpreteResult
            {
                IsSuccess = true,
                Message = $"Se elimino(aron) correctamente el(los) ingrediente(s) de la lista {luisResponse.Prediction.Entities.List.First()}",
                ResultModel = new ResultModel<object> { Value = true },
            });
        }
    }
}
