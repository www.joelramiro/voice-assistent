﻿// <copyright file="IInterpreteManager.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Interprete
{
    using System.Threading.Tasks;
    using VoiceAssistant.Core.Models.Interprete;
    using VoiceAssistant.Core.Settings.Contabilidad.Core;

    public interface IInterpreteManager
    {
        Task<ServiceResult<InterpreteResult>> Interprete(string text);
    }
}
