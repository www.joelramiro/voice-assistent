﻿// <copyright file="InterpreteManager.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Interprete
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using VoiceAssistant.Core.Interprete.LuisIntentService;
    using VoiceAssistant.Core.Models.Interprete;
    using VoiceAssistant.Core.Models.Interprete.Entities;
    using VoiceAssistant.Core.Settings.Contabilidad.Core;

    public class InterpreteManager : IInterpreteManager
    {
        private readonly HttpClient httpClient = new HttpClient();
        private readonly IEnumerable<IIntentResult> intentResults;

        public InterpreteManager(IEnumerable<IIntentResult> intentResults)
        {
            this.intentResults = intentResults;
        }

        public async Task<ServiceResult<InterpreteResult>> Interprete(string text)
        {
            try
            {
                var result = await this.httpClient.GetStringAsync($"https://app1congnitive.cognitiveservices.azure.com/luis/prediction/v3.0/apps/bd692753-9b51-4541-b708-793a9bec2495/slots/staging/predict?subscription-key=17df62f68f0d451487053ca70da9e9f8&verbose=true&show-all-intents=true&log=true&query={text}");
                var luisResponse = JsonConvert.DeserializeObject<LuisResponse>(result);

                //var intent = this.intentResults.FirstOrDefault(i => i.IntentName == luisResponse.Prediction.TopIntent);
                var intent = this.intentResults.FirstOrDefault(i => i.IntentName == "Mostrar_lista");

                var score = JsonConvert.DeserializeObject<JObject>(result);
                //var scoreValue = score.Value<JToken>("prediction").Value<JToken>("intents").Value<JToken>(luisResponse.Prediction.TopIntent).Value<double>("score");
                var scoreValue = 0.9;

                if (intent == null || scoreValue < 0.7)
                {
                    return ServiceResult<InterpreteResult>.ErrorResult(new[] { "Cannot interpret the text" });
                }


                var resultIntent = await intent.Execute(luisResponse);

                if (!resultIntent.Succeeded)
                {
                    var messages = resultIntent.ValidationMessages.Select(d => d.Value.First());
                    return ServiceResult<InterpreteResult>.ErrorResult(messages.ToArray());
                }

                return ServiceResult<InterpreteResult>.SuccessResult(resultIntent.Result);
            }
            catch (Exception e)
            {
                return ServiceResult<InterpreteResult>.ErrorResult(new[] { "Cannot receive a response from service" });
            }

            return ServiceResult<InterpreteResult>.ErrorResult(new[] { "Cannot interpret the text" });
        }
    }
}
