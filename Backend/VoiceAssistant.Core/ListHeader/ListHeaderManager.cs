﻿// <copyright file="ListHeaderManager.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.ListHeader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using VoiceAssistant.Core.Settings.Contabilidad.Core;
    using VoiceAssistant.Database.Repositories;

    public class ListHeaderManager : IListHeaderManager
    {
        private readonly IRepository<Database.Models.ListHeader> listHeaderRepository;

        public ListHeaderManager(IRepository<Database.Models.ListHeader> listHeaderRepository)
        {
            this.listHeaderRepository = listHeaderRepository;
        }

        public async Task<ServiceResult<bool>> CreateAsync(Database.Models.ListHeader listHeader)
        {
            if (listHeader == null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"The object {nameof(listHeader)} can't be null." });
            }

            var exist = await this.listHeaderRepository.FirstOrDefaultAsync(d => d.Description == listHeader.Description);
            if (exist != null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"A object with the name [{listHeader.Description}] already exist." });
            }

            if (listHeader.Details != null)
            {
                listHeader.Details = listHeader.Details.GroupBy(d => d.Description).Select(d => d.First()).ToList();
            }

            try
            {
                var result = this.listHeaderRepository.Create(listHeader);
                await this.listHeaderRepository.SaveChangesAsync();
                return ServiceResult<bool>.SuccessResult(true);
            }
            catch (Exception)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"Exception at created the registred" });
            }
        }

        public async Task<ServiceResult<bool>> DeleteByIdAsync(int id)
        {
            var listHeader = await this.listHeaderRepository.FirstOrDefaultAsync(d => d.Id == id);
            if (listHeader == null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"Not found a element with the id [{id}]." });
            }

            this.listHeaderRepository.Delete(listHeader);
            await this.listHeaderRepository.SaveChangesAsync();
            return ServiceResult<bool>.SuccessResult(true);
        }

        public async Task<ServiceResult<bool>> DeleteByNameAsync(string name)
        {
            name = name.ToLower();
            var listHeader = await this.listHeaderRepository.FirstOrDefaultAsync(d => d.Description.ToLower() == name);
            if (listHeader == null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"Not found a element with the name [{name}]." });
            }

            this.listHeaderRepository.Delete(listHeader);
            await this.listHeaderRepository.SaveChangesAsync();
            return ServiceResult<bool>.SuccessResult(true);
        }

        public async Task<ServiceResult<bool>> EditAsync(Database.Models.ListHeader listHeader)
        {
            if (listHeader == null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"The object {nameof(listHeader)} can't be null." });
            }

            var listHeaderOriginal = await this.listHeaderRepository.All().Include(d => d.Details).FirstOrDefaultAsync(d => d.Id == listHeader.Id);
            if (listHeaderOriginal == null)
            {
                return ServiceResult<bool>.ErrorResult(new[] { $"Not found a element with the id [{listHeader.Id}]." });
            }

            var newDescriptions = listHeader.Details.GroupBy(d => d.Description).Select(d => d.First());
            listHeaderOriginal.Details = newDescriptions.ToList();
            listHeaderOriginal.Description = listHeader.Description;
            this.listHeaderRepository.Update(listHeaderOriginal);
            await this.listHeaderRepository.SaveChangesAsync();

            return ServiceResult<bool>.SuccessResult(true);
        }

        public async Task<ServiceResult<IEnumerable<Database.Models.ListHeader>>> GetAllAsync()
        {
            var result = await this.listHeaderRepository.All().ToListAsync();
            return ServiceResult<IEnumerable<Database.Models.ListHeader>>.SuccessResult(result);
        }

        public async Task<ServiceResult<Database.Models.ListHeader>> GetByIdAsync(int id)
        {
            var result = await this.listHeaderRepository.FirstOrDefaultAsync(d => d.Id == id);
            if (result == null)
            {
                return ServiceResult<Database.Models.ListHeader>.ErrorResult(new[] { $"Not found a element with the id [{id}]." });
            }

            return ServiceResult<Database.Models.ListHeader>.SuccessResult(result);
        }

        public async Task<ServiceResult<Database.Models.ListHeader>> GetByIdWithDetailAsync(int id)
        {
            var result = await this.listHeaderRepository.All().Include(h => h.Details).FirstOrDefaultAsync(d => d.Id == id);
            if (result == null)
            {
                return ServiceResult<Database.Models.ListHeader>.ErrorResult(new[] { $"Not found a element with the id [{id}]." });
            }

            return ServiceResult<Database.Models.ListHeader>.SuccessResult(result);
        }

        public async Task<ServiceResult<Database.Models.ListHeader>> GetByNameAsync(string description)
        {
            var result = await this.listHeaderRepository.All()
                .Include(d => d.Details)
                .FirstOrDefaultAsync(d => d.Description == description);
            if (result == null)
            {
                return ServiceResult<Database.Models.ListHeader>.ErrorResult(new[] { $"Not found a element with the description [{description}]." });
            }

            return ServiceResult<Database.Models.ListHeader>.SuccessResult(result);
        }
    }
}
