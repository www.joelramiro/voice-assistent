﻿// <copyright file="IListHeaderManager.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.ListHeader
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using VoiceAssistant.Core.Settings.Contabilidad.Core;

    public interface IListHeaderManager
    {
        Task<ServiceResult<IEnumerable<Database.Models.ListHeader>>> GetAllAsync();

        Task<ServiceResult<Database.Models.ListHeader>> GetByIdAsync(int id);

        Task<ServiceResult<Database.Models.ListHeader>> GetByNameAsync(string description);

        Task<ServiceResult<Database.Models.ListHeader>> GetByIdWithDetailAsync(int id);

        Task<ServiceResult<bool>> CreateAsync(Database.Models.ListHeader listHeader);

        Task<ServiceResult<bool>> EditAsync(Database.Models.ListHeader listHeader);

        Task<ServiceResult<bool>> DeleteByIdAsync(int id);

        Task<ServiceResult<bool>> DeleteByNameAsync(string name);

    }
}
