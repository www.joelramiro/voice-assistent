﻿// <copyright file="ResultModel.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Models.Interprete
{
    public class ResultModel<TEntity>
    {
        public TEntity Value { get; set; }
    }
}
