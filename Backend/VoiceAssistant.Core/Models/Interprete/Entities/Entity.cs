﻿// <copyright file="Entity.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Models.Interprete.Entities
{
    using System.Collections.Generic;

    public class Entity
    {
        public IEnumerable<string> List { get; set; }

        public IEnumerable<string> Ingredient { get; set; }
    }
}