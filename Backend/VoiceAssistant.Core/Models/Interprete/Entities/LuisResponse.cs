﻿// <copyright file="LuisResponse.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Models.Interprete.Entities
{
    public class LuisResponse 
    {
        public Prediction Prediction { get; set; }
    }

}
