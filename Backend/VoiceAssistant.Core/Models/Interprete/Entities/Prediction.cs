﻿// <copyright file="Prediction.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace VoiceAssistant.Core.Models.Interprete.Entities
{
    public class Prediction
    {
        public string TopIntent { get; set; }

        public Entity Entities { get; set; }
    }
}