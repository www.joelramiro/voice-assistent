﻿// <copyright file="InterpreteResult.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

using VoiceAssistant.Core.Interprete.LuisIntentService;

namespace VoiceAssistant.Core.Models.Interprete
{
    public class InterpreteResult
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public TypeResult TypeResult { get; set; }

        public ResultModel<object> ResultModel { get; set; }
    }
}
