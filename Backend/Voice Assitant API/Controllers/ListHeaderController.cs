﻿// <copyright file="ListHeaderController.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace Voice_Assitant_API.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Voice_Assitant_API.Models.Api.V1;
    using VoiceAssistant.Core.ListHeader;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.Error;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.GeneralData;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.ListDetail;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.ListHeader;

    [Route("api/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class ListHeaderController : ControllerBase
    {
        private readonly IListHeaderManager listHeaderManager;

        public ListHeaderController(IListHeaderManager listHeaderManager)
        {
            this.listHeaderManager = listHeaderManager;
        }


        [HttpGet]
        [ProducesResponseType(typeof(ListItemDataTransferObject<ListHeaderDataTransferObject>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetAsync()
        {
            var result = await this.listHeaderManager.GetAllAsync();
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(result.Result.Select(r => new ListHeaderDataTransferObject
            {
                Id = r.Id,
                CreatedDate = r.CreatedDate,
                Description = r.Description,
            }));
        }

        [HttpGet("GetByIdAsync/{id}")]
        [ProducesResponseType(typeof(ListHeaderDataTransferObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var result = await this.listHeaderManager.GetByIdAsync(id);
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(new ListHeaderDataTransferObject
            {
                Id = result.Result.Id,
                CreatedDate = result.Result.CreatedDate,
                Description = result.Result.Description,
            });
        }

        [HttpGet("GetWithDetailByIdAsync/{id}")]
        [ProducesResponseType(typeof(ListHeaderWithDetailDataTransferObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetWithDetailByIdAsync(int id)
        {
            var result = await this.listHeaderManager.GetByIdWithDetailAsync(id);
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(new ListHeaderWithDetailDataTransferObject
            {
                Id = result.Result.Id,
                CreatedDate = result.Result.CreatedDate,
                Description = result.Result.Description,
                Details = result.Result.Details.Select(d => new ListDetailDataTransferObject
                {
                    Id = d.Id,
                    CreatedDate = d.CreatedDate,
                    Description = d.Description,
                    ListHeaderDescription = d.ListHeader?.Description,
                    IdListHeader = d.IdListHeader,
                }).ToList(),
            });
        }

        [HttpDelete("DeleteByIdAsync/{id}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> DeleteByIdAsync(int id)
        {
            var result = await this.listHeaderManager.DeleteByIdAsync(id);
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(true);
        }

        [HttpPost()]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateAsync(ListHeaderCreateDataTransferObject listHeaderDataTransferObject)
        {
            var date = DateTime.Now;
            var result = await this.listHeaderManager.CreateAsync(new VoiceAssistant.Database.Models.ListHeader
            {
                CreatedDate = date,
                Description = listHeaderDataTransferObject.Description,
                Details = listHeaderDataTransferObject.Details != null ? listHeaderDataTransferObject.Details.Select(d => new VoiceAssistant.Database.Models.ListDetail
                {
                    CreatedDate = date,
                    Description = d.Description,
                }).ToList() : new List<VoiceAssistant.Database.Models.ListDetail>(),
            });
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(true);
        }

        [HttpPost("Update")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> UpdateAsync(ListHeaderUpdateDataTransferObject listHeader)
        {
            var result = await this.listHeaderManager.EditAsync(new VoiceAssistant.Database.Models.ListHeader
            {
                Id = listHeader.Id,
                Description = listHeader.Description,
            });
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(true);
        }
    }
}
