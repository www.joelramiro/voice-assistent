﻿// <copyright file="InterpretesController.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace Voice_Assitant_API.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Voice_Assitant_API.Models.Api.V1;
    using Voice_Assitant_API.ResultConversion;
    using VoiceAssistant.Core.Interprete;
    using VoiceAssistant.Core.Models.Interprete;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.Error;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.GeneralData;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.Interprete;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.ListHeader;

    [Route("api/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class InterpretesController : ControllerBase
    {
        private readonly IInterpreteManager interpreteManager;
        private readonly IEnumerable<IResultConverter> resultConverters;

        public InterpretesController(
            IInterpreteManager interpreteManager,
            IEnumerable<IResultConverter> resultConverters)
        {
            this.interpreteManager = interpreteManager;
            this.resultConverters = resultConverters;
        }

        [HttpPost("InterpreteAndExecuteData")]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadGateway)]
        [ProducesResponseType(typeof(InterpreteResultDataTransferObject), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> InterpreteAndExecuteData(InterpreteDataTransferObject interpreteDataTransferObject)
        {
            if (interpreteDataTransferObject == null)
            {
                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Target = nameof(interpreteDataTransferObject),
                        Message = $"El {nameof(interpreteDataTransferObject)} no puede ser nulo.",
                    },
                });
            }

            if (string.IsNullOrEmpty(interpreteDataTransferObject.Text))
            {
                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Target = nameof(interpreteDataTransferObject.Text),
                        Message = $"El campo {nameof(interpreteDataTransferObject.Text)} no puede ser vacío",
                    },
                });
            }

            var result = await this.interpreteManager.Interprete(interpreteDataTransferObject.Text);


            if (!result.Succeeded)
            {
                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Target = nameof(interpreteDataTransferObject.Text),
                        Message = result.ValidationMessages.First().Value.First(),
                    },
                });
            }

            var resultSelected = this.resultConverters.FirstOrDefault(d => d.TypeResult == result.Result.TypeResult);

            if (resultSelected == null)
            {
                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Target = nameof(result.Result.TypeResult),
                        Message = "Cannot be serialized",
                    },
                });
            }

            return this.Ok(new InterpreteResultDataTransferObject
            {
                IsSuccess = result.Result.IsSuccess,
                Message = result.Result.Message,
                ResultModel = resultSelected.Converter(result.Result.ResultModel),
            });
        }
    }
}
