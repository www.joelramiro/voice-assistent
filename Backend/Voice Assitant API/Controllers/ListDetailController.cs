﻿// <copyright file="ListDetailController.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace Voice_Assitant_API.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Voice_Assitant_API.Models.Api.V1;
    using VoiceAssistant.Core.ListDetail;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.Error;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.GeneralData;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.ListDetail;

    [Route("api/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class ListDetailController : ControllerBase
    {
        private readonly IListDetailManager listDetailManager;

        public ListDetailController(IListDetailManager listDetailManager)
        {
            this.listDetailManager = listDetailManager;
        }

        [HttpGet("GetByHeaderId/{headerId}")]
        [ProducesResponseType(typeof(ListItemDataTransferObject<ListDetailDataTransferObject>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetByHeaderIdAsync(int headerId)
        {
            var result = await this.listDetailManager.GetAllByListHeaderIdAsync(headerId);
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(result.Result.Select(r => new ListDetailDataTransferObject
            {
                Id = r.Id,
                CreatedDate = r.CreatedDate,
                Description = r.Description,
                IdListHeader = r.IdListHeader,
                ListHeaderDescription = r.ListHeader?.Description,
            }));
        }


        [HttpGet("DeleteById/{id}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> DeleteByIdAsync(int id)
        {
            var result = await this.listDetailManager.DeleteByIdAsync(id);
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(true);
        }

        [HttpGet("GetByIdAsync/{id}")]
        [ProducesResponseType(typeof(ListDetailDataTransferObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var result = await this.listDetailManager.GetByIdAsync(id);
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(new ListDetailDataTransferObject
            {
                Id = result.Result.Id,
                IdListHeader = result.Result.IdListHeader,
                ListHeaderDescription = result.Result.ListHeader?.Description,
                CreatedDate = result.Result.CreatedDate,
                Description = result.Result.Description,
            });
        }

        [HttpPost()]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateAsync(ListDetailCreateDataTransferObject listDetailDataTransferObject)
        {
            var result = await this.listDetailManager.CreateAsync(new VoiceAssistant.Database.Models.ListDetail
            {
                CreatedDate = DateTime.Now,
                Description = listDetailDataTransferObject.Description,
                IdListHeader = listDetailDataTransferObject.IdListHeader,
            });
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(true);
        }

        [HttpPost("Update")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> UpdateAsync(ListDetailUpdateDataTransferObject listDetail)
        {
            var result = await this.listDetailManager.EditAsync(new VoiceAssistant.Database.Models.ListDetail
            {
                Id = listDetail.Id,
                Description = listDetail.Description,
            });
            if (!result.Succeeded)
            {
                var messages = new StringBuilder();
                foreach (var error in result.ValidationMessages)
                {
                    messages.Append(error.Value.First());
                }

                return this.BadRequest(new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ((int)HttpStatusCode.InternalServerError).ToString(),
                        Message = messages.ToString(),
                    },
                });
            }

            return this.Ok(true);
        }

    }
}
