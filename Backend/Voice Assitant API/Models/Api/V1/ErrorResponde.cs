﻿// <copyright file="ErrorResponde.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace Voice_Assitant_API.Models.Api.V1
{
    using VoiceAssistants.DataTransferObjectModel.Model.V1.Error;

    public class ErrorResponse
    {
        public Error Error { get; set; }
    }
}
