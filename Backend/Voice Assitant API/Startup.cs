// <copyright file="Startup.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace Voice_Assitant_API
{
    using Hangfire;
    using Hangfire.SqlServer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Versioning;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Options;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using System;
    using Voice_Assitant_API.ResultConversion;
    using Voice_Assitant_API.Settings;
    using VoiceAssistant.Core.Interprete;
    using VoiceAssistant.Core.Interprete.LuisIntentService;
    using VoiceAssistant.Core.ListDetail;
    using VoiceAssistant.Core.ListHeader;
    using VoiceAssistant.Database.Database.Context;
    using VoiceAssistant.Database.Models;
    using VoiceAssistant.Database.Repositories;
    using VoiceAssistant.Hangfire.ScheduledJobs;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = false;
                config.ReportApiVersions = true;
                config.ApiVersionReader = ApiVersionReader.Combine(new QueryStringApiVersionReader("api-version"), new HeaderApiVersionReader("api-version"));

            });
            services.AddVersionedApiExplorer(
               options =>
               {
                   options.GroupNameFormat = "'v'VVV";
                   options.SubstituteApiVersionInUrl = true;
               });
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            services.AddSwaggerGen(c =>
            {
                c.OperationFilter<SwaggerDefaultValues>();
            });

            services.AddHttpClient();

            services.AddHangfire(c =>
            c.UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection"),
            new SqlServerStorageOptions
            {
                CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                QueuePollInterval = TimeSpan.Zero,
                UseRecommendedIsolationLevel = true,
                DisableGlobalLocks = true,
            }));

            services.AddHangfireServer();

            services.AddDbContext<VoiceAssistantContext>(c => c.UseSqlite("Data Source=VoiceAssistantDatabase.db"));
            services.AddScoped<IScheduledJob, ScheduledJob>();

            services.AddScoped<IRepository<ListHeader>, ListHeaderRepository>();
            services.AddScoped<IRepository<ListDetail>, ListDetailRepository>();
            services.AddScoped<IListHeaderManager, ListHeaderManager>();
            services.AddScoped<IListDetailManager, ListDetailManager>();
            services.AddScoped<IInterpreteManager, InterpreteManager>();
            services.AddScoped<IIntentResult, CreateListIntent>();
            services.AddScoped<IIntentResult, DeleteListIntent>();
            services.AddScoped<IIntentResult, AddElementAtListIntent>();
            services.AddScoped<IIntentResult, ShowListsHeaderIntent>();
            services.AddScoped<IIntentResult, RemoveElementFromListIntent>();
            services.AddScoped<IIntentResult, CleanListIntent>();

            services.AddScoped<IResultConverter, BooleanResultConverter>();
            services.AddScoped<IResultConverter, ShowListsHeaderResultConverter>();
            services.AddScoped<IResultConverter, ShowListHeaderWithDetailResultConverter>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                using (var context = scope.ServiceProvider.GetRequiredService<VoiceAssistantContext>())
                {
                    context.Database.EnsureCreated();
                }
            }
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Voice_Assitant_API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHangfireDashboard();
            });
        }
    }
}
