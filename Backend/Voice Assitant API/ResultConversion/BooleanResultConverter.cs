﻿// <copyright file="AddElementAtListResultConverter.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace Voice_Assitant_API.ResultConversion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using VoiceAssistant.Core.Interprete.LuisIntentService;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.Interprete;

    public class BooleanResultConverter : IResultConverter
    {
        public TypeResult TypeResult => TypeResult.Boolean;

        public ResultModel Converter(object model)
        {
            var result = (VoiceAssistant.Core.Models.Interprete.ResultModel<object>)model;
            return new ResultModel
            {
                TypeResult = this.TypeResult,
                Value = result.Value,
            };
        }
    }
}
