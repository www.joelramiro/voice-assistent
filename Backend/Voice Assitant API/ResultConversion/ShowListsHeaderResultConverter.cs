﻿// <copyright file="ShowListsHeaderResultConverter.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace Voice_Assitant_API.ResultConversion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using VoiceAssistant.Core.Interprete.LuisIntentService;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.Interprete;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.ListDetail;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.ListHeader;

    public class ShowListsHeaderResultConverter : IResultConverter
    {
        public TypeResult TypeResult => TypeResult.ListHeader;

        public ResultModel Converter(object model)
        {
            try
            {
                var baseResult = (VoiceAssistant.Core.Models.Interprete.ResultModel<object>)model;
                var result = (IEnumerable<VoiceAssistant.Database.Models.ListHeader>)baseResult.Value;
                return new ResultModel
                {
                    TypeResult = this.TypeResult,
                    Value = result.Select(t => new ListHeaderDataTransferObject
                    {
                        CreatedDate = t.CreatedDate,
                        Description = t.Description,
                        Id = t.Id,
                    }),
                };
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
