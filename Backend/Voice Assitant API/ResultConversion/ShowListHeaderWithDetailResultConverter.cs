﻿// <copyright file="ShowListDetailResultConverter.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace Voice_Assitant_API.ResultConversion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using VoiceAssistant.Core.Interprete.LuisIntentService;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.Interprete;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.ListDetail;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.ListHeader;

    public class ShowListHeaderWithDetailResultConverter : IResultConverter
    {
        public TypeResult TypeResult => TypeResult.ListHeaderWithDetail;

        public ResultModel Converter(object model)
        {
            try
            {
                var baseResult = (VoiceAssistant.Core.Models.Interprete.ResultModel<object>)model;
                var result = (VoiceAssistant.Database.Models.ListHeader)baseResult.Value;
                return new ResultModel
                {
                    TypeResult = this.TypeResult,
                    Value = new ListHeaderWithDetailDataTransferObject
                    {
                        CreatedDate = result.CreatedDate,
                        Description = result.Description,
                        Details = result.Details == null ? new List<ListDetailDataTransferObject>() : result.Details.Select(d => new ListDetailDataTransferObject
                        {
                            CreatedDate = d.CreatedDate,
                            Description = d.Description,
                            ListHeaderDescription = d.ListHeader?.Description,
                            Id = d.Id,
                            IdListHeader = d.IdListHeader,
                        }),
                        Id = result.Id,
                    },
                };
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
