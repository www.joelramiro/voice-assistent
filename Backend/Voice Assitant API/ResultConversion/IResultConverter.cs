﻿// <copyright file="IResultConverter.cs" company="Proyecto Aplicaciones de Vanguardia">
// Copyright (c) Proyecto Aplicaciones de Vanguardia. All rights reserved.
// </copyright>

namespace Voice_Assitant_API.ResultConversion
{
    using VoiceAssistant.Core.Interprete.LuisIntentService;
    using VoiceAssistants.DataTransferObjectModel.Model.V1.Interprete;

    public interface IResultConverter
    {
        public TypeResult TypeResult { get; }

        public ResultModel Converter(object model);
    }
}
